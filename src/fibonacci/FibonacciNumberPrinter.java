package fibonacci;

import java.math.BigInteger;

/**
 * Fibonacci Number Printer - see task
 * 
 * @author   (your name(s)) 
 * @version  (a version number or a date)
 */
public class FibonacciNumberPrinter {
    
	
	public BigInteger computeFibonacciNumber(int index)
	{
		assert index >= 0 : "Illegal Argument: Index needs to be greater or equal to 0!"; 
   	    BigInteger first = BigInteger.ZERO;
    	BigInteger next = BigInteger.ONE;
    	BigInteger fi = BigInteger.ZERO;   	     
    	for(int i = 2; i <= index; i++) {
    		fi = first.add(next);
    		first = next;
    		next = fi;
    	} 
    	
    	return fi;
	}
	
    /**
     * printing first Fibonacci numbers (the classical way) from 1 to max index
     * 
     * @param maxIndex  number of Fibonacci numbers to be printed respectively index of last Fibonacci number in sequence
     */
    public void printFirstFibonacciNumbers( final int maxIndex ){
    	if(maxIndex < 0) { //don't print anything else than this error message
    		System.out.println("Die Anzahl an Fibonacci-Zahlen muss gr��er 0 betragen!");
    		return;
    	}
    	StringBuilder sb = new StringBuilder(); //just playing around with StringBuilder to get subscripted ns
    	for (char ch : String.valueOf(maxIndex).toCharArray()) { //convert every char in String of maxIndex to subscript
    		sb.append((char) ('\u2080' + (ch - '0')));
    	}
    	
    	System.out.print("f" + sb + " = ");  	
    	
   	    BigInteger first = BigInteger.ZERO;
    	BigInteger next = BigInteger.ONE;
    	System.out.print(first);
    	if(maxIndex > 0)
    		System.out.print(", " + next);
    	for(int i = 2; i <= maxIndex; i++) {
    		BigInteger fi = first.add(next);
    		System.out.print(", " + fi);
    		first = next;
    		next = fi;
    	}    	 
    }//method()
}//class
